Andy Chen's repository of LaTeX CVs

To compile the pdf:

```
cd <cvdir>
mkdir build
pdflatex -output-directory=build <main-tex-file>
```

To compile the bibliography:

```
cp <bib-file> ./build/
cd ./build
bibtex <main-tex-file-no-ext>
```
