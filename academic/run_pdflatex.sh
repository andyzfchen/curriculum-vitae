mkdir -p build
pdflatex -output-directory=build Chen_Academic.tex
pdflatex -output-directory=build Chen_Academic.tex
cp ./*.bib build/
cd build/
bibtex Chen_Academic
bibtex presentations
cd ../
pdflatex -output-directory=build Chen_Academic.tex
